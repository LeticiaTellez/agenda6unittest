# Pruebas Unitarias - Agenda 6
***Generación de pruebas de interfaz de usuario***

El presente proyecto sigue las instrucciones estipuladas en la `Agenda 6` de la clase Ingeniería de Software. La cual consiste en la creación de una pantalla en la cual se pueda escoger la operación matemática a realizar para posteriormente agregar los valores a calcular, enviando así el resultado correcto al usuario que ingresó dichos valores.

A la interfaz anteriormente mencionada se le aplicaron pruebas unitarias las cuales identificaban cierto criterios relacionados con el modelo, controlador, las respuestas que los métodos de este generaba, etc.

**Integrantes**
 - Amara López Galo
 - Yosselin Peña Contrera
 - Leticia Téllez Fargas
 - Brian Téllez Hernández
